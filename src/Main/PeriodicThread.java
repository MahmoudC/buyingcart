package Main;

public class PeriodicThread {
    private final Store store = Store.getInstance();
    private boolean isRunning = true;

    PeriodicThread(){
    }
    private void startStockThread(){
        while (isRunning){
            System.out.println();
            store.displayStock();
            System.out.println("===============");
            try{
                Thread.sleep(15000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    public void startThread(){
        Thread thread = new Thread(() -> startStockThread());
        thread.start();
    }

    public void stopStocThread(){
        isRunning = false;
    }
}
