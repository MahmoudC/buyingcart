package Main;

import java.util.Objects;

public class Product {

    private final double price;
    private final int stockProductNr;
    private final String manufacturer;
    private final String productType;

    public Product(double price, int stockProductNr, String manufacturer, String productType) {
        this.price = price;
        this.stockProductNr = stockProductNr;
        this.manufacturer = manufacturer;
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }

    public double getPrice() {
        return price;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    @Override
    public String toString() {
        return this.productType + " " + this.manufacturer + ", Price = " + this.price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 &&
                this.manufacturer.equalsIgnoreCase(product.manufacturer) &&
                this.productType.equalsIgnoreCase(product.productType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, stockProductNr, manufacturer, productType);
    }
}