package Main;

import java.time.LocalDate;
import java.util.Objects;

public class Client implements Comparable<Client> {

    private static final LocalDate ld = LocalDate.now();
    private final String name;
    private int registrationYear;

    //anul inregistrarii se genereaza automat atunci cand adaugi un client
    public Client(String name) {
        this.name = name;
        setRegistrationYear(ld.getYear());

    }

    public String getName() {
        return name;
    }

    public int getRegistrationYear() {
        return registrationYear;
    }

    public void setRegistrationYear(int registrationYear) {
        this.registrationYear = registrationYear;
    }

    @Override
    public String toString() {
        return this.name + " " + this.registrationYear;
    }

    @Override
    public int compareTo(Client client) {
        return this.name.compareToIgnoreCase(client.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Client client = (Client) o;
        return registrationYear == client.registrationYear &&
                name.equalsIgnoreCase(client.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, registrationYear);
    }
}
