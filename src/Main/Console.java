package Main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Console {
    private final Scanner scanner = new Scanner(System.in);
    private final Store store = Store.getInstance();
    private final ControlConsole controlConsole = new ControlConsole();
    private final PeriodicThread periodicThread = new PeriodicThread();

    Console() {
    }

    public void beginningDisplay() {
        System.out.println("The available products are: " + "Laptop, Phone, TV");
        System.out.println("================");
        periodicThread.startThread();

        try {
            Thread.sleep(70);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void orders() {
        do {
            System.out.println();
            System.out.println("The available commands are:" + "\n -ADD_CLIENT <name> \n -ADD_PRODUCT <clientName> <productType> \n " +
                    "-DELETE_PRODUCT <clientName> <productName> <Manufacturer> \n -DELETE_ALL_PRODUCTS <clientName> \n -DISPLAY_CLIENTS \n " +
                    "-DISPLAY_CART <clientName> \n -COMPLETE_DISPLAY \n -EXIT");
            System.out.println("Type a command: ");
            try {
                String comanda = scanner.next();
                switch (comanda) {
                    case "ADD_CLIENT":
                        String nume = scanner.next();
                        store.addClient(nume);
                        break;

                    case "ADD_PRODUCT":
                        controlConsole.readAddProduct(scanner);
                        break;

                    case "DELETE_PRODUCT":
                        controlConsole.readDeleteProduct(scanner);
                        break;

                    case "DELETE_ALL_PRODUCTS":
                        controlConsole.readDeleteTotalCart(scanner);
                        break;

                    case "DISPLAY_CLIENTS":
                        store.displayClients();
                        break;

                    case "DISPLAY_CART":
                        controlConsole.readDisplayCart(scanner);
                        break;

                    case "COMPLETE_DISPLAY":
                        store.completeDisplay();
                        break;

                    case "EXIT":
                        periodicThread.stopStocThread();
                        System.exit(0);
                        break;

                    default:
                        System.out.println("The command does not exist!");

                }
            } catch (NonExtistentClientException e) {
                System.out.println(e.getMessage());
            } catch (NumberFormatException e) {
                System.out.println("Please use only numbers for Price, Quantity, Processors Nr., Battery Dimension, Nr. Megapixels and Diagonal!");
                //
            } catch (InputMismatchException e) {
                System.out.println("Please type \"true\" for \"Yes\" and \"false\" for \"No\" in the fields for TouchScreen and Smart");
            }
        } while (true);
    }
}
