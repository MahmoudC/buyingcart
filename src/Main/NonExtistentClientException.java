package Main;

public class NonExtistentClientException extends Exception {

    public NonExtistentClientException(String message) {
        super(message);
    }
}
