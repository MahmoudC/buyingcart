package Main;


public class Main {
    private static final Store store = Store.getInstance();
    private static final Console console = new Console();

    public static void main(String[] args) {
        
        store.addStock(new Laptop(4, true, 500, store.getNrProductsStock(), "Apple", "LAPTOP"));
        store.addStock(new Laptop(4, true, 500, store.getNrProductsStock(), "Apple", "LAPTOP"));
        store.addStock(new Laptop(4, true, 500, store.getNrProductsStock(), "Apple", "LAPTOP"));
        store.addStock(new Laptop(4, true, 500, store.getNrProductsStock(), "Lenovo", "LAPTOP"));
        store.addStock(new Laptop(4, true, 500, store.getNrProductsStock(), "Sony", "LAPTOP"));
        store.addStock(new Laptop(4, true, 500, store.getNrProductsStock(), "Huawei", "LAPTOP"));
        store.addStock(new Laptop(4, true, 500, store.getNrProductsStock(), "Samsung", "LAPTOP"));
        store.addStock(new Phone(6, 50, 500, store.getNrProductsStock(), "Samsung", "Phone"));
        store.addStock(new Phone(6, 50, 500, store.getNrProductsStock(), "SONY", "Phone"));
        store.addStock(new TV(150, true, 500, store.getNrProductsStock(), "Samsung", "TV"));
        store.addStock(new TV(150, true, 500, store.getNrProductsStock(), "SONY", "TV"));

        console.beginningDisplay();
        console.orders();
    }
}
