package Main;

import java.util.Scanner;

public class ControlConsole {

    private final Store store = Store.getInstance();
    private String clientName;
    private String productType;
    private String manufacturer;

    ControlConsole() {
    }

    public void readAddProduct(Scanner scanner) throws NonExtistentClientException {
        clientName = scanner.next();
        productType = scanner.next();
        int price;
        int quantity;
        store.verifyClientExistence(clientName);

        if (productType.equalsIgnoreCase("LAPTOP")) {
            System.out.println("Please type the product's details (Price, Quantity, Processors Nr., TouchScreen):");
            price = Integer.parseInt(scanner.next());
            quantity = Integer.parseInt(scanner.next());
            manufacturer = scanner.next();
            int processorNr = Integer.parseInt(scanner.next());
            boolean hasTouchScreen = scanner.nextBoolean();

            Laptop laptop = new Laptop(processorNr, hasTouchScreen, price, store.getNrProductsStock(), manufacturer, productType);
            store.addProductsInCart(clientName, quantity, laptop);
        } else if (productType.equalsIgnoreCase("Phone")) {
            System.out.println("Please type the product's details (Price, Quantity, Manufacturer, Battery Dimension, Nr. Megapixels):");
            price = Integer.parseInt(scanner.next());
            quantity = Integer.parseInt(scanner.next());
            manufacturer = scanner.next();
            int batteryDim = Integer.parseInt(scanner.next());
            int nrMegapixels = Integer.parseInt(scanner.next());

            Phone phone = new Phone(batteryDim, nrMegapixels, price, store.getNrProductsStock(), manufacturer, productType);
            store.addProductsInCart(clientName, quantity, phone);
        } else if (productType.equalsIgnoreCase("TV")) {
            System.out.println("Please type the product's details (Price, Quantity, Manufacturer, Diagonal, Smart):");
            price = Integer.parseInt(scanner.next());
            quantity = Integer.parseInt(scanner.next());
            manufacturer = scanner.next();
            int diagonal = Integer.parseInt(scanner.next());
            boolean isSmart = scanner.nextBoolean();

            TV tv = new TV(diagonal, isSmart, price, store.getNrProductsStock(), manufacturer, productType);
            store.addProductsInCart(clientName, quantity, tv);
        } else {
            System.out.println("The product is unavailable!");
        }
    }

    public void readDeleteProduct(Scanner scanner) throws NonExtistentClientException {
        clientName = scanner.next();
        productType = scanner.next();
        manufacturer = scanner.next();
        store.verifyClientExistence(clientName);
        store.deleteProductsFromCart(clientName, productType, manufacturer);
    }

    public void readDeleteTotalCart(Scanner scanner) throws NonExtistentClientException {
        clientName = scanner.next();
        store.verifyClientExistence(clientName);
        store.emptyCart(clientName);
    }

    public void readDisplayCart(Scanner scanner) throws NonExtistentClientException {
        clientName = scanner.next();
        store.verifyClientExistence(clientName);
        store.displayCart(clientName);
    }
}
