package Main;

import java.util.Objects;

public class Laptop extends Product {

    private final int processorsNr;
    private final boolean hasTouchScreen;

    public Laptop(int processorsNr, boolean hasTouchScreen, double price, int stockProductNr, String manufacturer, String productType) {
        super(price, stockProductNr, manufacturer, productType);
        this.processorsNr = processorsNr;
        this.hasTouchScreen = hasTouchScreen;
    }

    public String printTouchScreen() {
        if (this.hasTouchScreen)
            return "Yes";

        return "No";
    }

    @Override
    public String toString() {
        return super.toString() + ", Processors nr. = " + this.processorsNr + ", Touch Screen = " + printTouchScreen();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        Laptop laptop = (Laptop) o;
        return processorsNr == laptop.processorsNr &&
                hasTouchScreen == laptop.hasTouchScreen;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), processorsNr, hasTouchScreen);
    }
}


