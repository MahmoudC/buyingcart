package Main;


import java.util.*;

public class Store {
    private static Store SINGLETON;
    private final TreeMap<Client, ArrayList<Product>> clientsList = new TreeMap<>();
    private final List<Product> stock = new ArrayList<>();

    private Store() {
    }

    public void addStock(Product p) {
        stock.add(p);
    }

    public void displayStock() {
        System.out.println("The stock contains the following products:");
        stock.forEach(System.out::println);
    }

    public void deleteStock(Product p, int quantity) {
        int incrementQuantity = 0;
        while (incrementQuantity < quantity) {
            stock.remove(p);
            incrementQuantity++;
        }
    }

    /*public void afisareCantitateStoc() {
        if (stoc.size() == 0)
            System.out.println("Stocul nu are produse!");
        else if (stoc.size() == 1)
            System.out.println("Stocul are " + stoc.size() + " Produs");
        else
            System.out.println("Stocul are " + stoc.size() + " Produse");
    }*/

    public int getNrProductsStock() {
        return stock.size();
    }

    public void addClient(String name) {
        clientsList.put(new Client(name), new ArrayList<>());
        System.out.println("A new client was added!");
    }

    public void verifyClientExistence(String name) throws NonExtistentClientException {
        if (clientsList.isEmpty()) {
            throw new NonExtistentClientException("The client was not added in the system!");
        }
        Set<Client> keys = clientsList.keySet();
        for (Client key : keys) {
            if (key.getName().equalsIgnoreCase(name)) {
                return;
            }
        }

        throw new NonExtistentClientException("The client doesn't exist!");
    }

    public boolean verifyStockQuantity(Product p, int quantity) {
        List<Product> copyStock = new ArrayList<>(stock);
        boolean hasEnoughQuantity = true;
        int sum = 0;
        while (copyStock.contains(p)) {
            copyStock.remove(p);
            sum++;
        }
        if (sum < quantity) {
            System.out.println("The stock doen't contain the requested quantity!");
            hasEnoughQuantity = false;
        }
        return hasEnoughQuantity;
    }

    public void addProductsInCart(String name, int quantity, Product p) {
        if (!stock.contains(p)) {
            System.out.println("The product is unavailable!");
            return;
        }
        if (!verifyStockQuantity(p, quantity)) {
            return;
        }
        if (quantity <= 0) {
            System.out.println("The quantity shoud not be 0 or a negative value!");
            return;
        }
        Set<Client> keys = clientsList.keySet();
        for (Client key : keys) {
            if (key.getName().equalsIgnoreCase(name)) {
                int incrementQuantity = 0;
                while (incrementQuantity < quantity) {
                    clientsList.get(key).add(p);
                    incrementQuantity++;
                }
                deleteStock(p, quantity);
                if (quantity == 1) {
                    System.out.println("A " + p.getProductType() + " " + p.getManufacturer() + " was added in the cart of " + key.getName());
                } else {
                    System.out.println(quantity + " " + p.getProductType() + " " + p.getManufacturer() + " were added in the cart of " + key.getName());
                }
            }
        }
    }

    public void deleteProductsFromCart(String name, String productType, String manufacturer) {
        Client client = new Client(name);
        Iterator<Product> iter = clientsList.get(client).iterator();
        if (iter.hasNext()) {
            Product product = iter.next();
            if (productType.equalsIgnoreCase(product.getProductType()) && manufacturer.equalsIgnoreCase(product.getManufacturer())) {
                iter.remove();
                stock.add(product);
                System.out.println("A " + product.getProductType() + " " + product.getManufacturer() + " was deleted from the cart of " + name);
            } else {
                System.out.println("The mentioned product product doesn't exist in the cart of " + name + " in order to be deleted!");
            }
        } else {
            System.out.println("The cart of " + name + " doesn't contain products in order to be deleted!");
        }
    }

    public void emptyCart(String name) {
        Set<Client> keys = clientsList.keySet();
        for (Client key : keys) {
            if (key.getName().equalsIgnoreCase(name)) {
                if (clientsList.get(key).isEmpty()) {
                    System.out.println("The cart of " + name + " is already empty!");
                } else {
                    stock.addAll(clientsList.get(key));
                    clientsList.get(key).clear();
                    System.out.println("The cart of " + name + " was emptied!");
                }
            }
        }
    }

    public void displayClients() {
        if (clientsList.isEmpty())
            System.out.println("No clients were added!");
        clientsList.forEach((e, z) -> System.out.println("Name: " + e.getName().toUpperCase() + ", Registration Year: " + e.getRegistrationYear()));
    }

    public void totalPayment(String name) {
        Set<Client> keys = clientsList.keySet();
        double sum = 0;
        for (Client key : keys) {
            if (key.getName().equalsIgnoreCase(name)) {
                for (Product p : clientsList.get(key)) {
                    sum = sum + p.getPrice();
                }
                System.out.println("The cart of " + name + " contains:");
                if (key.getRegistrationYear() < 2018) {
                    sum = sum - (sum * 10 / 100);
                    clientsList.get(key).forEach(System.out::println);
                    System.out.println("A 10% discount was applied to all the products!");
                } else {
                    clientsList.get(key).forEach(System.out::println);
                }
                System.out.println();
                System.out.println("The total payment is: " + sum);
            }
        }
    }

    public void displayCart(String name) {
        Set<Client> keys = clientsList.keySet();
        for (Client key : keys) {
            if (key.getName().equalsIgnoreCase(name)) {
                if (clientsList.get(key).isEmpty()) {
                    System.out.println("The cart of " + name + " is empty!");
                    System.out.println("=======");
                    return;
                } else {
                    totalPayment(name);
                }
            }
        }
    }

    public void completeDisplay() {
        if (clientsList.isEmpty()) {
            System.out.println("No clients were added!");
            return;
        }
        Set<Client> keys = clientsList.keySet();
        for (Client key : keys) {
            if (clientsList.get(key).isEmpty()) {
                System.out.println("Name: " + key.getName().toUpperCase() + ", Registration Year: " + key.getRegistrationYear());
                System.out.println("The cart of " + key.getName().toUpperCase() + " is empty!");
                System.out.println();
            } else {
                System.out.println("Name: " + key.getName().toUpperCase() + ", Registration Year: " + key.getRegistrationYear());
                totalPayment(key.getName().toUpperCase());
                System.out.println("================");
                System.out.println();
            }
        }
    }

    public static Store getInstance() {
        if (SINGLETON == null)
            SINGLETON = new Store();

        return SINGLETON;
    }
}
