package Main;

import java.util.Objects;

public class Phone extends Product {

    private final int batteryDimension;
    private final int megapixels;

    public Phone(int batteryDimension, int megapixels, double price, int stockProductNr, String manufacturer, String productType) {
        super(price, stockProductNr, manufacturer, productType);
        this.batteryDimension = batteryDimension;
        this.megapixels = megapixels;
    }

    @Override
    public String toString() {
        return super.toString() + ", Battery Dimension = " + this.batteryDimension + ", Number of megapixels = " + this.megapixels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        Phone phone = (Phone) o;
        return batteryDimension == phone.batteryDimension &&
                megapixels == phone.megapixels;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), batteryDimension, megapixels);
    }
}
