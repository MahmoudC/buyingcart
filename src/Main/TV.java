package Main;

import java.util.Objects;

public class TV extends Product {

    private final int diagonal;
    private final boolean isSmart;

    public TV(int diagonal, boolean smart, int price, int stockProductNr, String manufacturer, String productType) {
        super(price, stockProductNr, manufacturer, productType);
        this.diagonal = diagonal;
        this.isSmart = smart;
    }

    public String printSmart() {
        if (this.isSmart)
            return "Yes";

        return "No";
    }

    @Override
    public String toString() {
        return super.toString() + ", Diagonal = " + this.diagonal + ", Smart TV = " + printSmart();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        TV tv = (TV) o;
        return diagonal == tv.diagonal &&
                isSmart == tv.isSmart;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), diagonal, isSmart);
    }
}
